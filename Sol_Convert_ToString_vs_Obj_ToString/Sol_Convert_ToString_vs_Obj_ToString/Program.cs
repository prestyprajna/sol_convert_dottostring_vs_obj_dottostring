﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Convert_ToString_vs_Obj_ToString
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = null;
            new Test().Display(str);

            int val = 23;
            new Test().Display(val);
        }
    }

    public class Test
    {
        public void Display(object obj)
        {
            //object dot to string cannot handle null values

            //string val1 = obj.ToString();
            //Console.WriteLine(val1);



            //convert dot to string can handle null values

            string val2 = Convert.ToString(obj);
            Console.WriteLine(val2);
        }
    }
}
